#include "SourceB.h"
#include <sstream>

using namespace std;

unordered_map< int, int >  users;
unordered_map<int, int> cars;



void seifA(sqlite3* db, char* zErrMsg)
{

	cout <<"9 trying to buy car 4: " << carPurchase(9, 4, db, zErrMsg) << endl;
	cout << "12 trying to buy car 3: " << carPurchase(12, 3, db, zErrMsg) << endl;
	cout << "12 trying again to buy car 3: " << carPurchase(12, 3, db, zErrMsg) << endl;

}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int rc;
	bool available;

	stringstream cmd;


	cmd << "select * from cars where id=" << carid;

	rc = sqlite3_exec(db, cmd.str().c_str(), getCar, &available, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	if (!available)
	{
		return false;
	}

	auto car = cars.find(carid);

	if (car == cars.end())
	{
		return false;
	}

	cmd.str(string());
	cmd << "select * from accounts where id=" << buyerid;

	rc = sqlite3_exec(db, cmd.str().c_str(), getUser, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}


	auto buyer = users.find(buyerid);

	if (buyer == users.end())
	{
		cars.erase(car);
		return false;
	}

	if (buyer->second >= car->second)
	{
		buyer->second -= car->second;
		cmd.str(string());
		cmd << "update accounts set balance=" << buyer->second << " where Buyer_id=" << buyerid;

		rc = sqlite3_exec(db, cmd.str().c_str(), NULL, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return false;
		}

		cmd.str(string());
		cmd << "update cars set available=0 where id=" << carid;

		rc = sqlite3_exec(db, cmd.str().c_str(), NULL, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			cars.erase(car);
			system("Pause");
			return false;
		}

		users.erase(buyer);
		cars.erase(car);
		return true;
	}

	return false;


}


int getUser(void* notUsed, int argc, char** argv, char** azCol)
{
	pair<int, int> user;

	for (int i = 0; i < argc; i++)
	{
		if (strcmp(azCol[i], "buyer_id") == 0)
		{
			user.first = atoi(argv[i]);
		}

		else if (strcmp(azCol[i], "balance") == 0)
		{
			user.second = atoi(argv[i]);
		}
	}

	users.insert(user);

	return 0;
}


int getCar(void* isAvailable, int argc, char** argv, char** azCol)
{
	pair<int, int> car;
	bool* available = (bool*)isAvailable;

	for (int i = 0; i < argc; i++)
	{
		if (strcmp(azCol[i], "id") == 0)
		{
			car.first = atoi(argv[i]);
		}

		else if (strcmp(azCol[i], "price") == 0)
		{
			car.second = atoi(argv[i]);
		}

		else if (strcmp(azCol[i], "available") == 0)
		{
			*available = ((argv[i])[0] == '1');
		}
	}

	if (*available)
	{
		cars.insert(car);
	}
	
	return 0;
}


void seifB(sqlite3* db, char* zErrMsg)
{
	cout << "Transferring from 1 to 3 500 bucks:" << balanceTransfer(1, 3, 500, db, zErrMsg) << endl;
	auto user = users.find(1);
	if (user != users.end())
	{
		cout << "1`s balance: " << user->second << endl;
	}

	cout << "Transferring from 1 to 3 300000 bucks:" << balanceTransfer(1, 3, 300000, db, zErrMsg) << endl;
	if (user != users.end())
	{
		cout << "1`s balance: " << user->second << endl;
	}

}
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	stringstream cmd;
	int rc;

	auto fromUser = users.find(from);

	if (fromUser == users.end())
	{
		cmd << "select * from accounts where id=" << from;

		rc = sqlite3_exec(db, cmd.str().c_str(), getUser, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return false;
		}

	}

	cmd.str(string());

	fromUser = users.find(from);

	if (fromUser == users.end() || fromUser->second < amount)
	{
		cout << "Transaction error." << endl;
		system("Pause");
		return false;

	}


	auto toUser = users.find(to);

	if (toUser == users.end())
	{
		cmd << "select * from accounts where id=" << to;

		rc = sqlite3_exec(db, cmd.str().c_str(), getUser, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return false;
		}

	}

	cmd.str(string());

	toUser = users.find(to); 
	if (toUser == users.end())
	{
		cout << "Transaction error." << endl;
		system("Pause");
		return false;

	}
	


	fromUser->second -= amount;
	toUser->second += amount;

	cmd.str(string());

	cmd << "begin transaction";

	rc = sqlite3_exec(db, cmd.str().c_str(), NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	cmd.str(string());
	cmd << "update accounts set balance=" << fromUser->second << " where id=" << from;

	rc = sqlite3_exec(db, cmd.str().c_str(), NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);

		cmd.str(string());
		cmd << "rollback" << from;
		rc = sqlite3_exec(db, cmd.str().c_str(), NULL, 0, &zErrMsg);
		system("Pause");
		return false;
	}

	cmd.str(string());
	cmd << "update accounts set balance=" << toUser->second << " where id=" << to;

	rc = sqlite3_exec(db, cmd.str().c_str(), NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);

		cmd.str(string());
		cmd << "rollback" << from;
		rc = sqlite3_exec(db, cmd.str().c_str(), NULL, 0, &zErrMsg);
		system("Pause");
		return false;
	}

	cmd.str(string());
	cmd << "commit";

	rc = sqlite3_exec(db, cmd.str().c_str(), NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	return true;

}