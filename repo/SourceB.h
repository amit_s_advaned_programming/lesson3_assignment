#pragma once
#include <iostream>
#include <string>
#include "sqlite3.h"
#include <unordered_map>

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
int getCar(void* isAvailable, int argc, char** argv, char** azCol);
int getUser(void* notUsed, int argc, char** argv, char** azCol);
void seifA(sqlite3* db, char* zErrMsg);
void seifB(sqlite3* db, char* zErrMsg);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);