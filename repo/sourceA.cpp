#include "sourceA.h"

void SeifA()
{
	int rc;
	sqlite3* db;
	char* zErrMsg = 0;

	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)
	{
		cout << "Error: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("pause");
		return;
	}

	rc = sqlite3_exec(db, "create table people(id integer primary key autoincrement, name text) ", NULL, 0, &zErrMsg);


	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}

	rc = sqlite3_exec(db, "insert into people(name) values(\"Amit\")", NULL, 0, &zErrMsg);


	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}
	rc = sqlite3_exec(db, "insert into people(name) values(\"Gali\")", NULL, 0, &zErrMsg);


	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}

	rc = sqlite3_exec(db, "insert into people(name) values(\"Itai\")", NULL, 0, &zErrMsg);


	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}

	rc = sqlite3_exec(db, "update people set name=\"geller\" where name=\"Itai\"", NULL, 0, &zErrMsg);


	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}

	sqlite3_close(db);
}